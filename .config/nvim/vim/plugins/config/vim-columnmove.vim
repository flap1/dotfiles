nmap      <LocalLeader>f       <Plug>(columnmove-f)
xmap      <LocalLeader>f       <Plug>(columnmove-f)
nmap      <LocalLeader>w       <Plug>(columnmove-w)
xmap      <LocalLeader>w       <Plug>(columnmove-w)
" omap      J       <Plug>(columnmove-w)
" omap     vJ      v<Plug>(columnmove-w)
" omap     VJ      V<Plug>(columnmove-w)
" omap <C-v>J  <C-v><Plug>(columnmove-w)
nmap      <LocalLeader>F      <Plug>(columnmove-F)
xmap      <LocalLeader>F      <Plug>(columnmove-F)
nmap      <LocalLeader>b      <Plug>(columnmove-b)
xmap      <LocalLeader>b      <Plug>(columnmove-b)
" omap      K      <Plug>(columnmove-b)
" omap     vK     v<Plug>(columnmove-b)
" omap     VK     V<Plug>(columnmove-b)
" omap <C-v>K  <C-v><Plug>(columnmove-b)

let g:columnmove_no_default_key_mappings = 1

