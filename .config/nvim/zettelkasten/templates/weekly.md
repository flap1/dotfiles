---
title: {{year}}-W{{week}}
date:  {{hdate}}
---

# Review Week {{week}} / {{year}}

---

## Highlights

- **this**!
- that!

## {{monday}} (Monday)
## {{tuesday}} (Tuesday)
## {{wednesday}} (Wednesday)
## {{thursday}} (Thursday)
## {{friday}} (Friday)
## {{saturday}} (Saturday)
## {{sunday}} (Sunday)
