# -------------------------------------------------------------------------
# profile init
# -------------------------------------------------------------------------
if [ "$ZSHRC_PROFILE" != "" ]; then
  zmodload zsh/zprof
fi

# -------------------------------------------------------------------------
# base configuration
# -------------------------------------------------------------------------
source-safe() { if [ -f "$1" ]; then source "$1"; fi }
source "$ZSHDIR/base.zsh"

# -------------------------------------------------------------------------
# Mappings
# -------------------------------------------------------------------------
source "$ZSHDIR/mappings.zsh"


# -------------------------------------------------------------------------
# Options
# -------------------------------------------------------------------------
source "$ZSHDIR/options.zsh"


# -------------------------------------------------------------------------
# Completion
# -------------------------------------------------------------------------
source "$ZSHDIR/completion.zsh"


# -------------------------------------------------------------------------
# Function
# -------------------------------------------------------------------------
source "$ZSHDIR/function.zsh"


# -------------------------------------------------------------------------
# Aliase
# -------------------------------------------------------------------------
source "$ZSHDIR/alias.zsh"


# -------------------------------------------------------------------------
# Plugin
# -------------------------------------------------------------------------
source "$ZSHDIR/plugins/init.zsh"


# -------------------------------------------------------------------------
# Post Execution
# -------------------------------------------------------------------------
source "$ZSHDIR/post_load.zsh"


# -------------------------------------------------------------------------
# Execute Local Script
# -------------------------------------------------------------------------
source-safe "$ZHOMEDIR/.zshrc.local"


# -------------------------------------------------------------------------
# profile end
# -------------------------------------------------------------------------
if [ "$ZSHRC_PROFILE" != "" ]; then
  zprof
fi
export VOLTA_HOME="$HOME/.volta"
export PATH="$VOLTA_HOME/bin:$PATH"
