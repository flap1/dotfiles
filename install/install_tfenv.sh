#!/bin/bash

# https://github.com/tfutils/tfenv
[ -d "$HOME/.tfenv" ] && git clone --depth=1 https://github.com/tfutils/tfenv.git ~/.tfenv
